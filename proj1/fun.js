createLabel = function(name){
	var node 		= document.createElement('label');
	node.innerHTML  = name;
	return node;
}
var obj

var getId = (var counter = 1;
              return function(){
              	counter = counter + 1;
              	return counter;
              } )()

var data = {}

function Base(fname,tname){
	//this.counter   = 0
	this.fname 	   = fname
	this.tname	   = tname	
//	this.formItems = [["name","text"], ["age","number"]];
	
}

Base.prototype.formItems = [["name","text"], ["age","number"]];

Base.prototype.validate = {
               text  : /[a-z]{4,6}/,
               num: /[0-9]{2,3}/
           }

Base.prototype.performValidation = function(){
          var testPass = 1
          var self = this
          $('input :visible').each(function(idx,val){
                     if($(val).attr('type')=='text')
                         testPass = self.validate['text'].test($(val).val())
                     else if($(val).attr('type')=='num')
                         testPass = self.validate['number'].test($(val).val())
                	
            })
          if(testPass==0)
           alert("lol");
         return testPass
      }
 
Base.prototype.createForm = function(formId){
	var form = $('#'+this.fname)[0];
	var header="<tr> "
	this.formItems.forEach(function(item){
		form.appendChild(createLabel(item[0]));
		var el = document.createElement("input");
		el.type = item[1];
		el.name = item[0];
		form.appendChild(el);
		header = header + "<th>" + item[0] + "</th>"
		form.appendChild(document.createElement('br'));
	})
	header += "</tr>"
	$('#'+this.tname).append(header)
}


function Student(fname,tname){
	Base.call(this,fname,tname);
	newItems = [ ["marks","number"],
				 ["sub","text"]
			   ]
	this.formItems = this.formItems.concat(newItems)
	// var self =this
	// newItems.forEach(function(item){
	// 	self.formItems.push(item);
	// })
	//this.formItems.concat(newItems);
	//console.log(this.formItems)
}

function Employee(fname,tname){
	 Base.call(this,fname,tname);
     this.formItems.push(["email","email"]);

}
Employee.prototype = Object.create(Base.prototype);
Employee.prototype.constructor = Employee; 
Student.prototype  = Object.create(Base.prototype);
Student.prototype.constructor = Student;

Employee.prototype = new Base();


add = function(formId){
    if(obj.performValidation()==false)
             return false;
	$('#table :visible').unbind('click');
	var row='',record=[];

	

	$('#' + obj.fname).find("input").each(function(idx,el){
		record.push(el.value);
		row = row + '<td>' + el.value + '</td>';
	})
	obj.data.push(record)

	$('#' + obj.tname).append("<tr>" + row + ' <td><button class="edit" onclick = "edit()"> Edit   </button> </td>  <td>  	<button class="del"> Delete </button></td>'+ "</tr>");
	
	$('#'+obj.tname +" .del").click(function(){
		del($(this).closest('tr'));
	})

	$('#' + obj.tname + " .edit").click(function(){
		row = $(this).closest('tr')
		edit(row )
		$('[name="modify"]').attr('idx',row.index())
	})	
	
}


function del(row){
  obj.data.splice(row.index()-1,1);
  row.remove();
}

function edit(row){
  $(row).find('td').each( function(idx,dataEl){
      var inputField =$('#' + obj.fname).find('input')[idx]
      if(inputField)
        inputField.value = dataEl.innerHTML
   })
  $('button[name="modify"]').prop('disabled',false);
}

function saveChanges(){
  $('#' + obj.fname).find('input').each( function(idx,dataEl){
  	var ridx =$('[name="modify"]').attr('idx') 
   var row = $('#' + obj.tname).find('tr')[ridx];
   obj.data[ridx-1][idx] = dataEl.value
   $(row).find('td')[idx].innerHTML = dataEl.value
  })
  
   $('button[name="modify"]').prop('disabled',true);
}


onload = function(){
	stud = new Student('sform', 'stable');
	empl = new Employee('eform', 'etable');
	stud.createForm('student'); empl.createForm('employee');

	$('select').on('change',function(){
             
             if($(this).val()=='student'){
                   window.obj =stud;
                   $('#student').show();
                   $('#employee').hide();
                 }
	     else if($(this).val()=='employee'){
                   window.obj = empl;
                   $('#employee').show();
                   $('#student').hide();
		} 
         });
    $('.del').click(function(){
    	del(this);
    })   
}

