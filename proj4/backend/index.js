
const Hapi      = require('hapi')
const routes    = require('./routes.js')
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');

const server = new Hapi.Server();

server.connection({port:8000,
    routes:{cors:true}})

routes.forEach((route, idx)=>{
    server.route(route)
})

const options = {
    info: {
            'title': 'Test API Documentation',
            
        }
    };

server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], (err) => {
        server.start( (err) => {
           if (err) {
                console.log(err);
            } else {
                console.log('Server running at:', server.info.uri);
            }
        });
    });

