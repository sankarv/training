fs          = require('fs')
utility     = require('./Utility.js')
const DbDriver  = require('./dbDriver')
const dbDriver  = DbDriver.getInstance()
addSnippet  = (request, reply) =>{
   
    payload = request.payload     
    payload.createdDate  = Date()
    payload.id           = utility.genId()
    payload.lastModified = payload.createdDate

    console.log("addSnippet" + JSON.stringify(payload))

    dbDriver.insertSnippet(payload).then(()=>{
             reply({ msg :"Added id:" + payload.id}).code(201) //created
    },(err) =>{
        reply({msg:"i Didnt insert your code...."}).code(400)
    })
    //reply({ msg :"Added id:" + payload.id}).code(201) //created
    
}

updateSnippet  = (request, reply) =>{
    payload = request.payload;
  
     payload.lastModified = Date()
     dbDriver.updateSnippet(payload.user, request.params.id, payload).then((report)=>{
         if(report.result)
            reply({msg:'updated'}).code(201)
        else
           reply({msg :'Did you mean to add new snippet ?'}).code(401)

     }) 

}

fetchSnippet    = (request, reply) =>{
    console.log("Fetching Snippet : " + request.params.id);  

    dbDriver.getSnippet(request.query.user, request.params.id).then((report)=>{
        if(report.result ==false)
         reply({msg:"Code illa"}).code(404) // no content
        else
          reply(report.data[0]).code(200)
    })    
}

fetchAll       =(request, reply) =>{
    console.log("Fetching Snippet for user: " + request.query.user)
   
    dbDriver.getSnippet(request.query.user).then((report)=>{
        if(report.result==false)
          reply({msg:"User doesnt exist"}).code(403)
        else
          reply(report.data).code(200)
    })
}

removeSnippet  = (request, reply)=>{
    console.log("Removing Snippet id:" + request.params.id)
    user =  request.payload.user
    id   =  request.params.id
   
    let result = dbDriver.delSnippet(user, id)

    result.then((report)=>{
        if(report.result)
         reply({msg:"Deleted "}).code(200)    
        else
         reply({msg:"Something Went wrong"}).code(400)
    }, (msg)=>{
    console.log("Fail" + msg)
    })
   
       
 
    
}
module.exports = {
    addSnippet,
    updateSnippet,
    fetchSnippet,
    fetchAll,
    removeSnippet
}