const r             = require('rethinkdb')
const co            = require('co')
const schemas       = require('../Schema/schemas')
const Joi           = require('joi') 
var   _conn ;

promisify = (query, conn) =>{
    return new Promise(function (resolve, reject)  {
        query.run(conn,(err,result) =>{
            if(err)
             reject(err)
            else
            resolve(result)
        })
    })
}


var _dbDriver;
class DbDriver {
    constructor (){
        if(_dbDriver)
            throw ("Its is a Singleton class.... use getInstance() instead")
         this.connPromise = this.initConnection()
    }

    initConnection(){
         let me = this
         return co(function* (){
            _conn =  yield r.connect({host: 'localhost',port: 28015, db:'Training'})
            me.conn = _conn

        })       
    }
    static getInstance(){
        if(_dbDriver == undefined)
          _dbDriver = new DbDriver()
        return _dbDriver
        
    }
    insertSnippet (snippet){
        return co(function* (){
            let record = {
                id       : snippet.id,
                user     : snippet.user,
                snippet  : snippet
            }
            Joi.validate(record,schemas.dbSchema, (err,val)=>{
                if(err)
                throw err
                console.log(val)
            })
            yield r.table('Snippets').insert(record).run(_conn)
    
        })
    }

    updateSnippet (user, id, snippet){
        return co(function* (){
            var report = yield r.table('Snippets').filter({user:user,id:id}).update({snippet}).run(_conn)
            return {result:report.replaced==1}
        })
    }
    getSnippet (user, id){

        return co(function* (){
              let fil = {"user": user}
              if(id)
               fil['id']  = id 
              var cur = yield  r.table("Snippets").filter(fil).getField("snippet").run(_conn)
              let res = yield cur.toArray();
              if(res.length !=0)
              return {result:true,data:res}
              else
              return {result: false,data:res}
        })
        
    }

    delSnippet (user,id){
        return co(function* () {
            try{
                let report = yield r.table('Snippets').filter({user:user,id:id}).delete().run(_conn)
                return ({result:report.deleted==1})

            }
            catch (err){
                return ({result:false})
            }
        })
    }
}

module.exports = DbDriver