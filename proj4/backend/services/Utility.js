const fs        = require('fs')
const config    = require('../config.json')
const r         = require('rethinkdb')
const DbDriver  = require('./dbDriver')
const dbDriver  = DbDriver.getInstance()

genId       = () =>{
    return Math.random().toString(36).substr(2,4)
}

getSnippets  = ()=>{
    throw ("Deprecated....  Time to move on dude... use dbDriver instead :P")
    var snippets = fs.readFileSync(config.dataLocation,"utf-8")
    return JSON.parse(snippets)
}

putSnippets  = (snippets)=>{
    throw ("Deprecated..... use dbDriver instead :P")
    dbDriver.insertSnippet(snippets)
    fs.writeFile(config.dataLocation, JSON.stringify(snippets))
}

module.exports = {
    getSnippets,
    putSnippets,
    genId
}