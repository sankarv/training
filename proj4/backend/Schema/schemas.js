Joi     = require('joi')
Config  = require('../config.json')

const snippetsPost = Joi.object({
        'code'      : Joi.string().required(),
        'user'      : Joi.string().valid(Config.allowedUsers).required(),
        'lang'      : Joi.string().required()
 //       'id'        : Joi.string().required()
    })
const snippetsUpdate = Joi.object().keys({
        'code'      : Joi.string().required(),
        'user'      : Joi.string().valid(Config.allowedUsers).required(),
        'lang'      : Joi.string().required(),
        'id'        : Joi.string().required()
    })

const dbSchema      = Joi.object({
        'id'        : Joi.string().required(),
        'user'      : Joi.string().valid(Config.allowedUsers).required(),
        'snippet'   : snippetsUpdate
        .keys({
            'lastModified' : Joi.any(),
            'createdDate' : Joi.any()
        })
})


module.exports ={
    snippetsPost,
    snippetsUpdate,
    dbSchema
}