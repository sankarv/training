
Snippets        = require('./services/Snippets')
Schemas         = require('./Schema/schemas')
Config          = require('./config.json')
routes = [{
        method      : 'POST',
        path        : '/snippets/',
        handler     : (request,reply) =>{
                    Snippets.addSnippet(request, reply)
        },
        
        config      :{
            tags: ['api'],
            cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        },
            validate    : {
                payload     : Schemas.snippetsPost
            } 
        } 
},{
        method      : 'GET',
        path        : '/snippets/{id}',
        handler     : (request, reply) =>{
                    Snippets.fetchSnippet(request,reply)
        },
        
        config      :{
            tags: ['api'],
              cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        },
            validate    : {
                query     : {                      
                        'user'      : Joi.string().valid(Config.allowedUsers).required()
                },
                params    : {
                        'id'    : Joi.string().required()
                }
            } 
        } 
},{
        method      : 'GET',
        path        : '/snippets/',
        handler     : (request, reply) =>{
                    Snippets.fetchAll(request, reply)
        },
         config      : {
                 tags           : ['api'],
                 validate       :{
                        query   : {
                                'user'  : Joi.string().required()
                        }
                 }
         }
},{
        method      : 'DELETE',
        path        : '/snippets/{id}',
        handler     : (request, reply) =>{
                        Snippets.removeSnippet(request, reply)
                    },
        config      : {
                tags     :['api'],
                validate :{
                        payload  :{
                                'user' : Joi.string().required()
                        }
                }
        }
},{
        method      : 'PUT',
        path        : '/snippets/{id}',
        handler     : (request, reply) =>{
                Snippets.updateSnippet(request, reply)
        },
        config      :{
                tags            :['api'],
                validate        :{
                        payload : Schemas.snippetsUpdate
                }

        }
}]

module.exports = routes