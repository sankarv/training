const myApp     = angular.module('myApp',[])

myApp.controller('formController',['$scope','$http',($scope, $http)=>{
    errorCallback = (response) =>{
        errorData ={
            msg         : response.data,
            statusCode  : response.status,
            statusText  : response.statusText
        }
        $scope.displayData = errorData
    }
    successCallback = (response)=>{
            console.log(response.data)
            $scope.displayData = response.data
    }
    $scope.get = ()=>{
        config = {
            method      : 'GET',
            url         : 'http://localhost:8000/snippets/' + ($scope.id ||''),
            params      : {
                user    : $scope.user
            }            
        }
        $http(config).then(successCallback, errorCallback)
    }

    $scope.del = () =>{
        config = {
            method    : 'DELETE',
            url       : 'http://localhost:8000/snippets/' + $scope.id,
            data      :{
                user    : $scope.user
            }
        }
        $http(config).then(successCallback, errorCallback)
    }

    $scope.add = (update) =>{
        config = {
            method  : 'POST',
            url     : 'http://localhost:8000/snippets/',
            data    : {
                code    : $scope.code,
                lang    : $scope.lang,
                user    : $scope.user               

            }
        }
        if(update){
         config.data.id = $scope.id
         config.method  = 'PUT' 
         config.url    = config.url + $scope.id
        }
        $http(config).then(successCallback, errorCallback)
    }


}])