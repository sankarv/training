var myApp	= angular.module('myApp',['ngRoute'])

myApp.factory('CountryData',function($http,$q){
	var data ={}, name
	var promise =  $q(function(resolve, reject){
		$http.get("http://country.io/names.json").then(function(response){
		name = response.data;
		for(var key in name)
			data[name[key]] = {id: key}		

		return $http.get(" http://country.io/iso3.json")
		}).then(function(response){
			add(response.data, "Code")
			return  $http.get('http://country.io/capital.json')

		}).then(function(response){
			add(response.data, "Capital")
			resolve(data)
		})

	})

	
	add = function(newData, propName){
		for(key in newData)
			data[name[key]][propName] = newData[key]
	}
	return promise


})



myApp.config(['$routeProvider',function($routeProvider){
	$routeProvider.when('/country/:country',{
		template	: 'ID :{{data.id}} <br> ISO Code : {{data.Code}} <br> Capital: {{data.Capital}}',
		controller  : 'displayController'
		 
	})	
	.when('/',{
		template   : 'Nothing to display',
		controller : 'bodyController'
	})
}])



myApp.filter('objectFilter',function(){
	return function(countryData,searchTerm){
		var filteredData = {}
		angular.forEach(countryData,function(value,key){
			if(key.search(searchTerm)==0)
				filteredData[key] = value
		})
		return filteredData
	}
})

myApp.controller('bodyController',['$scope','CountryData', function($scope, countryData){
	$scope.searchTerm	= 'Quick search';
	countryData.then(function(data){
		$scope.countryData 	= data
	})
	
	
}]).
controller('displayController',['$scope', 'CountryData', '$routeParams', function($scope,CountryData, $routeParams){
	CountryData.then(function(data){
		$scope.data = data[$routeParams.country]
	})
	

}])