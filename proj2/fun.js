
onload = function(){
	 window.data 	= {}
	 FormTemplate	= Handlebars.compile( $('#Form').html())
	 TableTemplate  = Handlebars.compile( $('#Tabledata').html())
	 $('select').on('change',function(){
             
             if($(this).val()=='student'){
                   Student.prototype.render()
                   populateFullData(Student)
                 }
	     else if($(this).val()=='employee'){
                   Employee.prototype.render()
                   populateFullData(Employee)
		} 
         });
	 $('#render').on('click','.delete',function(){
	 	del($(this))
	 })

	 $('#render').on('click','.edit',function(){
	 	edit($(this).attr('objId'))
	 })

	 $('#render').on('click','.modify',function(){
	 	id = $(this).attr('objId')
	 	if(id==undefined || id==null)
	 		throw('How is this happening!!!!!!')
	 	modify($(this).attr('objId'))
	 })
}

genId	= (function(){
			var counter = 0;
			return function(){
				counter = counter +1;
				return counter;
			}
	})()

function Base(id, name, age){
	this.id 	= id;
	this.name	= name;
	this.age	= age;
	this.valid  = true;
}

Base.prototype.inputs = [{
			name	 : "name",
			type	 : "text"
		},{
			name	 : "age",
			type	 : "number"
		}]

Base.prototype.render  =  function(){
	$('#render').html(FormTemplate({inputs:this.inputs}))
}

function Student(id,name,age,mark){
	Base.call(this,id, name, age);
	this.mark = mark;
}

Student.prototype 				= Object.create(Base.prototype)
Student.prototype.constructor	= Student
Student.prototype.inputs		= Student.prototype.inputs.concat([{
 									name: "mark",
 									type: "number"
 								}])


function Employee(id, name, age, email){
	Base.call(this, id, name, age)
	this.email = email;
}

Employee.prototype				= Object.create(Base.prototype)
Employee.prototype.constructor  = Employee
Employee.prototype.inputs		= Employee.prototype.inputs.concat([{
 									name: "email",
 									type: "email"
 								}])

populateFullData = function(formName){
	 for(var prop in window.data)
	 	if(data[prop].valid && (data[prop] instanceof formName))
	 		populateTable(data[prop])
} 
populateTable 	 = function(obj){
	var rowData = []
	$('.table').find('th').each(function(idx,header){
		rowData.push(obj[$(header).html()])
	})
	$('.table').append(TableTemplate({id:obj.id,
									  data:rowData}))

}
add	= function(){
	
	var obj;
	if($('select').val()=='student')
		obj = new Student(genId());
	else
		obj = new Employee(genId());
	$('.form').find('input').each(function(id,inputEl){
		obj[$(inputEl).attr('name')] = $(inputEl).val()
	})

	window.data[obj.id] = obj;
	populateTable(obj);	

	
}

del	= function(rowObj){
	var id = rowObj.attr('objId')
	window.data[id].valid = false;
	rowObj.closest('tr').remove()

}

edit = function(id){
	$('.modify').attr('disabled',false)
	$('.form').find('input').each(function(idx,prop){
		$(prop).val(data[id][$(prop).attr('name')]);
	})

	$('.modify').attr('objId',id);
}

modify = function(id){
	$('.modify').attr('disabled',true)
	$('.modify').removeAttr('objid')
	row = $('.edit[objid=' + id + ']').closest('tr').find('td')
	$('.form').find('input').each(function(idx,prop){
		row[idx].innerHTML = $(prop).val()
		data[id][$(prop).attr('name')] = $(prop).val()
	})

}
